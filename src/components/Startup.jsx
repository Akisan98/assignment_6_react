import React, {useEffect, useState} from 'react'
import { useUserContext } from '../contexts/UserContext'
import {useHistory} from "react-router-dom";
import styles from './style/startup.module.css'
import {useTranslationContext} from "../contexts/TranslationContext";
import {getUserByName, postUser} from "../api/translations_API";
import UserInput from './UserInput';
import { Header } from './Header';

export const Startup = () => {

    console.log('Startup.render')

    const history = useHistory()
    const [ inputText, setInputText ] = useState('')
    const [ user, setUser ] = useUserContext()
    const { dispatch } = useTranslationContext()

    const AuthUser = async (username) => {
        // Check if Existing User
        console.log("Auth")
        await getUserByName(username)
            .then(response => {
                if (response.length === 0) {
                    // Not User - Add Them
                    console.log("Auth - NEW")
                    postUser(username, [])
                        .then(postResponse => {
                            console.log("Auth - Add To Tranlations Context - New User")
                            console.log(response)
                            console.log(postResponse)
                            dispatch({ type: 'SET_TRANSLATION', payload: [postResponse]})
                        })
                } else {
                    // Exists
                    console.log("Auth - Add To Tranlations Context - Existing User")
                    console.log(response)
                    dispatch({ type: 'SET_TRANSLATION', payload: response})
                }
            })
    }

    const HandleUserInput = (event) => {
        setInputText(event.target.value)
    }

    const HandleButtonClicked = () => {
        setUser(inputText)
        localStorage.setItem('username', inputText)
    }

    useEffect(() => {
        console.log('Startup.useEffect')
        if (user !== null) history.push("/translate")
        if (inputText !== '') {
            AuthUser(inputText).then(
                history.push("/translate")
            )
        }
    }, [user])

    return (
        <>
        <div className={styles.loginContainer}>
            <img className={styles.logoImg} src="/lostInTranslation/Logo.png" width="75px" height="75px"  alt="Logo Image"/>
            <div className={styles.title}>
                <h1>Lost in Translation</h1>
                <h4>Get started</h4>
            </div>
            <div className={styles.userInput}>
                <UserInput className={styles.userInput} inputOnChange={ HandleUserInput } buttonOnClick={ HandleButtonClicked } placeholderText={"Enter Your Name"} buttonText={"Log In"}/>
            </div>
          {/*<input onChange={ HandleUserInput } className={styles.usernameInput} placeholder="username"/>
          <button onClick={ HandleButtonClicked } className={styles.loginButton}>log in</button>*/}
        </div>
        </>
    )
}
