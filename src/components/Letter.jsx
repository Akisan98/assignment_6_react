import React from 'react'

const Letter = (props) => {

    const boxStyle = {
        color: "green",
        alignContent: "center",
        justifyContent: "center",
        width: "150px",
        margin: "0",
        float:"left",
        padding: "2%"
    }

    return (
        <div style={boxStyle}>
            <img src={props.img} width="150px" height="150px"/>
            <p style={{textAlign: "center", fontSize: "2.5rem", fontWeight: "800", width: "150px", color: "blueviolet"}}>{props.text}</p>
        </div>
    )
}

export default Letter
