import React, {createContext, useContext, useReducer} from "react";

const TranslationContext = createContext(null)

export const useTranslationContext = () => {
    return useContext(TranslationContext)
}

const translationReducer = (state, action) => {
    switch (action.type) {
        case 'SET_TRANSLATION':
            return {
                loading: false,
                error: '',
                translation: action.payload
            }
        case 'SET_TRANSLATION_ERROR':
            return {
                ...state,
                loading: false,
                error: action.payload
            }
        case 'RESET_ALL':
            return {
                ...state,
                loading: true,
                error: '',
                translation: []
            }
        default:
            return state
    }
}

const initialState = {
    loading: true,
    error: '',
    translation: []
}

const TranslationProvider = ({children}) => {

    const [ translationState, dispatch ] = useReducer(translationReducer, initialState)

    return (
        <TranslationContext.Provider value={{ translationState, dispatch }}>
            {children}
        </TranslationContext.Provider>
    )
}

export default TranslationProvider
