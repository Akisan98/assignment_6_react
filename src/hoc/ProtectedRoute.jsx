import React , {useEffect} from "react";
import {Redirect, Route} from "react-router-dom";
import {useUserContext} from "../contexts/UserContext";
import {useTranslationContext} from "../contexts/TranslationContext";
import { getUserByName } from "../api/translations_API";

const ProtectedRoute = ({component, path}) => {
    const [ user, setUser ] = useUserContext()
    const { dispatch } = useTranslationContext()

    useEffect(() => {
        // on mount 
        let localUsername = localStorage.getItem('username');
        if (localUsername !== "null") {
            setUser(localUsername)
            getUserByName(localUsername)
                .then(response => {
                    dispatch({ type: 'SET_TRANSLATION', payload: response})
                })
        }
    }, [])

    console.log(`USER: ${user}`)
    console.log(user !== null)
    return user !== null
        ? <Route component={component} path={path}/>
        : <Redirect to="/"/>
}

export default ProtectedRoute